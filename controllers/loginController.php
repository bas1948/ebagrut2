<?php



class LogIn extends Controller{
    function __construct(){
        parent::__construct();
        Session::init();      
    }

    public function Index(){
        $logged = Session::get('loggedin');
        $permission = Session::get('permission');
        if($logged == false)
        {
            $this->view->breadcrumb = array('Home' => URL, 'Log In' => '');
            $this->view->pageName = 'Log In - Ebagrut';
            $this->view->render('login/index');
        }
    }

    public function run(){
        $result = $this->model->run($_POST['email'],Hash::create('md5',$_POST['pword'],HASH_KEY));

        if($result == 'default')
        {
            header('location:'.URL.'student');
            return;
        }
        elseif($result == 'admin')
        {
            header('location:'.URL.'dashboard');
            return;
        }
        elseif ($result == 'root') {
            header('location:'.URL.'user');
            return;
        }
        elseif ($result == 'login_fail') {
            header('location:'.URL.'login');
            return;
        }
        return;
    }

    public function logout(){
        Session::destroy();
        header('location:'.URL.'login');
        exit;
    }

}