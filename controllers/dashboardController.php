<?php

class DashBoard extends Controller{
    function __construct(){
        parent::__construct();
        Session::init();
        $logged = Session::get('loggedin');
        $permission = Session::get('permission');
        if($logged == false)
        {
            Session::destroy();
            header('location:'.URL.'login');
            exit;
        }
        if($permission == 'default')
        {
            header('location:'.URL.'error/permissionError');
        }
    }

    //render dashboard page.

    function Index($status = ''){

        $level = Session::get('level');
        $teacherID = Session::get('user_id');
        $permission = Session::get('permission');

        $result = $this->getGroup($level, $teacherID, $permission);
        $name = Session::get('fname');

        if(!empty($status))
            $this->view->status = $status;

        $this->view->breadcrumb = array('Home' => URL, 'Dashboard' => URL.'dashboard');
        $this->view->pageName = 'DashBoard - '.$name.' - Ebagrut';
        $this->view->Group = $result['info'];
        $this->view->render('dashboard/index');
    }

    public function getGroup($level, $teacherID, $permission)
    {
        if($permission == 'admin')
            return $this->model->getAdminGroup($level,$teacherID);

        if($permission == 'root')
            return $this->model->getRootGroup();
    }

    public function grammarEdit(){
        $name = Session::get('fname');
        if(isset($_POST['GrammarLevel']))
        {
            $level = $_POST['GrammarLevel'];

            $grammar = $this->model->getGrammarTable($level);

            $this->view->grammarTable = $grammar['info'];
        }

        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'Grammar Edit' => '');
        $this->view->pageName = 'Grammar Edit - '.$name.' - Ebagrut';
        $this->view->render('dashboard/grammarEdit');
    }

    public function readingEdit(){
        $name = Session::get('fname');
        if(isset($_POST['readingLevel']))
        {
            $level = $_POST['readingLevel'];
            $reading = $this->model->getReadingTable($level);
            $this->view->readingTable = $reading['info'];
        }

        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'Reading Edit' => '');
        $this->view->pageName = 'Reading Edit - '.$name.' - Ebagrut';
        $this->view->render('dashboard/readingEdit');

        return;
    }

    public function studentEdit($id){
        $data = $this->model->getUser($id);
        $this->view->SingleUser = $data['info'];
        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'Progress Edit' => '');
        $this->view->render('dashboard/studentEdit');
        return;
    }

    public function editProgress($id){
        $data = array();

        $data['progress'] = $_POST['progress'];
        $data['level'] = $_POST['level'];
        $data['count_grammar'] = 0;
        $data['count_reading'] = 0;

        $this->model->updateProgress($id,$data);
        $this->Index();
    }

    public function studentNote($id){
        $this->view->userID = $id;
        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'Add Note' => '');
        $this->view->render('dashboard/addNote');
    }

    public function addNote($id){
        $data['title'] = $_POST['note_title'];
        $data['content'] = $_POST['note_content'];
        $data['teacherID'] = Session::get('user_id');
        $data['sentBy'] = $data['teacherID'];
        //gets time from time function in Library/Controller
        $data['date_added'] = $this->NOW();
        $data['studentID'] = $id;
        $status = $this->model->addNote($data);
        $this->Index($status);
    }
    public function GrammarExcEdit($id){
        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'Grammar Edit Exercise' => URL.'dashboard/grammarEdit', $id => '');
        $excInfo = $this->model->SelectSingleGrammar($id);
        $this->view->excInfo = $excInfo['info'];
        $this->view->render('dashboard/GrammarExcEdit');
    }

    public function GrammarExcDelete($id){
        $this->model->GrammarExcDelete($id);
        header('location:'.URL.'dashboard/grammarEdit');
        return;
    }

    public function grammarEditSave($id){

        if (isset($_POST) && !empty($_POST)) {
            //$data is an Array
            $data['level'] = $_POST['level'];
            $data['question'] = $_POST['question'];
            $data['answer1'] = $_POST['answer1'];
            $data['answer2'] = $_POST['answer2'];
            $data['answer3'] = $_POST['answer3'];
            $data['correct_answer'] = $_POST['correct_answer'];

            $this->model->grammarEditSave($id,$data);
            $this->grammarEdit();
        }
    }

    public function grammarAdd(){

        if (isset($_POST) && !empty($_POST)) {
            //$data is an Array
            $data['level'] = $_POST['level'];
            $data['question'] = $_POST['question'];
            $data['answer1'] = $_POST['answer1'];
            $data['answer2'] = $_POST['answer2'];
            $data['answer3'] = $_POST['answer3'];
            $data['correct_answer'] = $_POST['correct_answer'];
            
            $this->model->grammarAdd($data);
        }

        header('location:'.URL.'dashboard/grammarEdit');
        return;
    }

    public function readingExcAdd(){
        $name = Session::get('fname');
        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'Reading Edit' => URL.'dashboard/readingEdit', 'Add Reading Comprehension Exercise' => '');
        $this->view->pageName = 'Reading Add - '.$name.' - Ebagrut';
        $this->view->render('dashboard/readExcAdd');
    }

    public function readingAdd(){

        if (isset($_POST) && !empty($_POST)) {
            //$data is an Array
            $data['level'] = $_POST['level'];
            $data['article'] = $_POST['article'];
            $data['question1'] = $_POST['question1'];
            $data['question2'] = $_POST['question2'];
            $data['question3'] = $_POST['question3'];
            $data['question4'] = $_POST['question4'];
            $data['question5'] = $_POST['question5'];
            $data['article_desc'] = substr($data['article'], 0, 600);
            $this->model->readingAdd($data);
        }

        header('location:'.URL.'dashboard/readingEdit');
        return;
    }
    public function readingEditSave($id){

        if (isset($_POST) && !empty($_POST)) {
            //$data is an Array
            $data['level'] = $_POST['level'];
            $data['article'] = $_POST['article'];
            $data['question1'] = $_POST['question1'];
            $data['question2'] = $_POST['question2'];
            $data['question3'] = $_POST['question3'];
            $data['question4'] = $_POST['question4'];
            $data['question5'] = $_POST['question5'];
            $data['article_desc'] = substr($data['article'], 0, 350);

            $this->model->readingEditSave($id,$data);
        }

        header('location:'.URL.'dashboard/readingEdit');
        return;
    }

    public function readingExcEdit($id){

        $excInfo = $this->model->getReadExc($id);
        $this->view->excInfo = $excInfo['info'][0];
        /*print_r($excInfo);
        die();*/
        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'Reading Comprehension Edit Exercise' => URL.'dashboard/grammarEdit', $id => '');
        $this->view->render('dashboard/readingExcEdit');

    }

    public function readingExcDelete($id){
        $this->model->readingExcDelete($id);
        header('location:'.URL.'dashboard/readingEdit');
        return;
    }

    public function inboxTitle(){
        $id = Session::get('user_id');
        $this->view->Messages = $this->model->getMsgTitle($id)['info'];
        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'Inbox' => '');
        $this->view->render("dashboard/adminInbox");
    }

    public function msgContent($NoteId){
        $this->view->content = $this->model->msgContent($NoteId)['info'][0];

        if($this->view->content['sentBy'] == Session::get('user_id'))
            $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'Outbox' => URL.'dashboard/outboxTitle' ,$this->view->content['title'] => '');
        else
            $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'Inbox' => URL.'dashboard/inboxTitle' ,$this->view->content['title'] => '');

        $this->view->render("dashboard/msgContent");
    }

    public function SendNote($studentID){
        $this->view->studentID = $studentID;
        $this->view->render("dashboard/SendNote");
    }

    public function DelNote($noteID){
        $this->model->DelNote($noteID);
        header('location:'.URL.'dashboard/inboxTitle');
    }

    public function outboxTitle(){
        $id = Session::get('user_id');
        $this->view->Messages = $this->model->getOutbox($id)['info'];
        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'OutBox' => '');
        $this->view->render("dashboard/adminOutbox");
    }

    public function studentAnswers($id){
        $this->view->studAnswers = $this->model->getStudAnswers($id)['info'][0];
        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard',$this->view->studAnswers['first_name']." ".$this->view->studAnswers['last_name']." Answers" => '');
        $this->view->render("dashboard/studentAnswers");
    }

    public function readingScore($studentID, $excID){
        header("location:".URL."dashboard");
    }

    public function uploadExamsPage($error = ""){
        $this->view->error = $error;
        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'dashboard', 'Exams' => '');
        $this->view->pageName = 'Exams - Dashboard - Ebagrut';
        $examList = $this->model->getExamsList()['info'];
        usort($examList, array($this,"sortByModule"));
        $this->view->examList = $examList;
        $this->view->render("dashboard/uploadExams");
    }

    public function uploadExam(){

        if(!empty($_POST))
        {
            $data['module'] = $_POST['module'];
            $data['term'] = $_POST['term'];
            //get date, format to sql
            $Date = $_POST['day'].'/'.$_POST['month'].'/'.$_POST['year'];   
            $Date = date('Y-m-d',strtotime($Date));
            //end date handling
            $data['date'] = $Date;
            $data['file_path'] = "exams/".$_FILES['exam']['name'];

            if($_FILES['exam']['error'])
            {
                $this->uploadExamsPage("A server error occured, please send it to your admin.".$_FILES['exam']['error']);
                return false;
            }
            if($_FILES['exam']['size'] > 10000000)
            {
                $this->uploadExamsPage("File size exceeds limit");
                return false;
            }
            if(pathinfo($_FILES['exam']['name'],PATHINFO_EXTENSION) != "pdf")
            {
                $this->uploadExamsPage("Sorry, only <strong>PDF</strong> files allowed.");
                return false;
            }
            if(!file_exists("exams"))
            {
                if(mkdir("exams", 0755,true))
                {
                    if(!move_uploaded_file($_FILES["exam"]["tmp_name"], "exams/".$_FILES['exam']['name']))
                    {
                        $this->uploadExamsPage("Failed to upload file, please try again or contant your admin1");
                        return false;
                    }
                    else
                    {
                        $this->model->uploadExam($data);
                        header("location:".URL."dashboard/uploadExamsPage");
                        return true;
                    }
                }
                else
                {
                    $this->uploadExamsPage("Failed to upload file, please try again or contant your admin2");
                    return false;
                }
            }
            else
            {
                if(!move_uploaded_file($_FILES["exam"]["tmp_name"], "exams/".$_FILES['exam']['name']))
                {
                    $this->uploadExamsPage("Failed to upload file, please try again or contant your admin3");
                    return false;
                }
                else
                {
                    $this->model->uploadExam($data);
                    header("location:".URL."dashboard/uploadExamsPage");
                    return true;
                }
            }
        }//end if !empty post
    }

    public function sortByModule($a, $b) {
        if ($a == $b)
            return 0;
        return strcmp($a['module'], $b['module']);
    }
}