<?php

//This is root controller

class User extends Controller{

    public function __construct(){
        parent::__construct();
        Session::init();
        $logged = Session::get('loggedin');
        $permission = Session::get('permission');

        if($logged == false)
        {
            Session::destroy();
            header('location:'.URL.'login');
            exit;
        }
        if($logged == 'true')
        {
            if($permission == 'admin')
                header('location:'.URL.'dashboard');

            if($permission == 'default')
                header('location:'.URL.'error/permissionError');
        }
    }
    public function Index(){
        $data = $this->model->userList();
        $this->view->userList = $data['info'];

        $name = Session::get('fname');

        $this->view->pageName = 'Root DashBoard - '.$name;

        $this->view->render('user/userlist');
    }

    public function create() {

        $data = array();

        $data['fname'] = $_POST['fname'];
        $data['lname'] = $_POST['lname'];
        $data['email'] = $_POST['email'];
        $data['pword'] = $_POST['pword'];
        $data['ver_pword'] = $_POST['ver_pword'];
        $data['permission'] = $_POST['permission'];

        if($this->model->check($data) > 0)
        {
            header('location:'.URL.'user');
            exit;
        }

        $this->model->create($data);
        header('location:'.URL.'user');

        /* PERFORM ERROR CHECKING
        if($pword == $ver_pword)
        {
            echo "PWORD MATCH";
        }
        else
        {
            header('location: ../user');
        }*/
    }

    public function edit($id){
        $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'student', 'Edit User' => '');
        
        //fetch indivual user
        $data = $this->model->UserSingleList($id);

        $this->view->user = $data['info'];

        $this->view->render('user/edit');
    }

    public function delete($id){
        $this->model->delete($id);
        header('location:'.URL.'user');
    }

    public function editSave($id){

        $data = array();

        $data['fname'] = $_POST['fname'];
        $data['lname'] = $_POST['lname'];
        $data['email'] = $_POST['email'];
        $data['pword'] = $_POST['pword'];
        $data['ver_pword'] = $_POST['ver_pword'];
        $data['permission'] = $_POST['permission'];
        $data['level'] = $_POST['level'];

        $this->model->editSave($data,$id);

        header('location:'.URL.'user');
    }

    public function tempSQL(){
        $this->view->render('user/tempSQL');
    }

    public function Query(){
        $this->view->result = $this->model->temp($_POST['SQL']);
        $this->view->render('user/tempSQL');
    }
}