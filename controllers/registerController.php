<?php

class register extends Controller{
    
    public function __construct(){
        parent::__construct();        
    }
    
    public function Index($error = ''){
        Session::init();
        $logged = Session::get('loggedin');
        
        if($logged == true)
            header('location:'.URL.'student');
        else
        {
            $this->view->breadcrumb = array('Home' => URL, 'Register' => '');
            $this->view->pageName = 'Register - Ebagrut';
            $this->view->error = $error;
            $this->view->render('register/index');
        }
    }
    
    public function create() {
        //get date and convert it to MYSQL Requirement
        $DOB = $_POST['day'].'/'.$_POST['month'].'/'.$_POST['year'];   
        $DOB = date('Y-m-d',strtotime($DOB));
        //insert all $_POST Data into its own array
        $data = array();
        $data['fname'] = $_POST['fname'];
        $data['lname'] = $_POST['lname'];
        $data['email'] = $_POST['email'];
        $data['pword'] = $_POST['pword'];
        $data['ver_pword'] = $_POST['ver_pword'];
        $data['permission'] = 'default';
        $data['birthDay'] = $DOB;

        //user checking
        if($data['pword'] == $data['ver_pword'])
        {

            if($this->model->check($data) > 0)
            {
                $this->Index($error = 'User Already Exists!');
                return;
            }
            $success = $this->model->create($data);
            if ((int)$success > 0)
            {
                $this->view->success = 'Registeration successful! Log-In and start preparing..!';
                $this->view->render('login/index');
                return;
            }
            $this->Index();
            return;
        }
        else
        {
            $this->Index($error='Passwords Do Not Match!');
            return;
        }
    }
}