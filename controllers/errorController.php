<?php 

class Error extends Controller{

    function __construct() {
        parent::__construct();
    }
    //render error page, typically 404.php.. if this page isn't available, redirect to homepage.
    public function Index(){
        if(file_exists('views/error/index.php'))
        {
            $name = Session::get('fname');
            $this->view->breadcrumb = array('Home' => URL, 'Error - 404' => '');
            $this->view->pageName = 'Error 404 - Ebagrut';
            $this->view->render('error/404');
        }
        else
        {
            header('location:'.URL.'index');
        }
    }

    public function permissionError(){
        $name = Session::get('fname');
        $this->view->breadcrumb = array('Home' => URL, 'Error - 403' => '');
        $this->view->pageName = 'Error 403 - Ebagrut';
        $this->view->render('error/permission');
    }
}
