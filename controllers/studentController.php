<?php



class Student extends Controller{

    function __construct(){
        parent::__construct();
        Session::init();
        $logged = Session::get('loggedin');
        $permission = Session::get('permission');
        if($logged == false)
        {
            Session::destroy();
            $this->view->render('index/index');
            exit;
        }
    }

    public function Index($updated = false){

        $id = Session::get('user_id');
        //Method for after user finishes sorting exam to get updated information about him.
        if($updated == true)
            $this->model->ReSelect($id);
        //get student informationm
        $name = Session::get('fname');
        $level = Session::get('level');

        $this->view->pageName = 'Home - '.$name.' - Ebagrut'; //Get user name, send data for header 'title'
        $this->view->level = $level;
        $this->view->name = $name; //Send student name to page
        $this->view->progress = Session::get('progress');//send student progress to page
        //check student level, if no level assigned, redirect to sorting exam.
        if($level < 3)
        {
            $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'student', 'Sorting Exam' => '');
            //Get Grammar exercises for sorting exam
            //$this->view->shuffle = $$this->shuffle_assoc();
            $this->view->grammar = $this->Grammar()['info'];
            $this->view->render('student/sorting');
            return;
        }
        else//if level assigned, redirect to student home page
        {
            $this->view->render('student/index');
            return;
        }
    }
    //Get grammar exercises for sorting exam,function should be expanded
    public function Grammar(){
        return $this->model->getGrammar('sorting');
    }
    //Function for sorting formula
    public function Sorting(){
        $id = Session::get('user_id');
        $sum = 2.5;
        foreach($_POST as $k=>$v)
            if($v == 'correct_answer')
                $sum += 6.5;

            if($sum < 60)
                $this->model->setLevel($id,'3');

            if($sum >= 60 && $sum < 80)
                $this->model->setLevel($id,'4');

            if($sum >= 80 && $sum <= 100)
                $this->model->setLevel($id,'5');

            $this->Index(true);

            return;
        }
    //funciton to get Grammar score
        public function GrammarScore(){
            $id = Session::get('user_id');
            $sum = 0;
            foreach($_POST as $k=>$v)
                if($v == 'correct_answer')
                    $sum += 10;

            if($sum < 60)
            {
                header('location:'.URL.'student');
                return;
            }
            elseif ($sum > 60) {
                $this->model->setGrammarProgress($id, Session::get('count_grammar')+1,Session::get('progress')+25);
                $this->model->ReSelect($id);
                header('location:'.URL.'student');
                return;
            }
        }

        public function saveAnswers($excID){

                if (isset($_POST) && !empty($_POST)) {
                    $data=array();
                    $i=1;
                    foreach ($_POST as $key => $value) {
                        $data['answer'.$i] = $value;
                        $i++;
                    }
                    $data['excID'] = $excID;
                    $data['studentID'] = Session::get('user_id');
                    $this->model->setReadingProgress($data['studentID'], Session::get('count_reading')+1);
                    $this->model->ReSelect($data['studentID']);
                    $this->model->saveAnswers($data);
                }
                header('location:'.URL.'student');
            }

            public function inboxTitle(){
                $id = Session::get('user_id');
                $this->view->Messages = $this->model->getMsgTitle($id)['info'];
                $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'student', 'Inbox' => '');
                $this->view->render("student/studInbox");
            }

            public function msgContent($NoteId){
                $this->view->content = $this->model->msgContent($NoteId)['info'][0];

                if($this->view->content['sentBy'] == Session::get('user_id'))
                    $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'student', 'Outbox' => URL.'student/outboxTitle' ,$this->view->content['title'] => '');
                else
                    $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'student', 'Inbox' => URL.'student/inboxTitle' ,$this->view->content['title'] => '');

                $this->view->render("student/msgContent");
            }

            public function SendNote($teacherID){
                $this->view->teacherID = $teacherID;
                $this->view->render("student/SendNote");
            }

            public function AddStudNote($teacherID){
                $data['title'] = $_POST['note_title'];
                $data['content'] = $_POST['note_content'];
                $data['teacherID'] = $teacherID;
                //gets time from time function in Library/Controller
                $data['date_added'] = $this->NOW();
                $data['studentID'] = Session::get('user_id');
                $data['sentBy'] = $data['studentID'];
                $this->model->addNote($data);
                $this->inboxTitle();
            }

            public function outboxTitle(){
                $id = Session::get('user_id');
                $this->view->Messages = $this->model->getOutbox($id)['info'];
                $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'student', 'OutBox' => '');
                $this->view->render("student/studOutbox");
            }

            public function MessageTeacher(){
                $this->view->admins = $this->model->getAdmins()['info'];
                $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'student', 'Message Teacher' => '');
                $this->view->render("student/MessageTeacher");
            }
            public function SendTeacher(){
                if(!empty($_POST) && isset($_POST['teacherID']))
                    $this->SendNote($_POST['teacherID']);
            }

            public function DelNote($noteID){
                $this->model->DelNote($noteID);
                header('location:'.URL.'student/inboxTitle');
            }

            public function readingPage(){
                $reading = $this->model->getReadingExc(Session::get('level'));

                if($reading['count'] > 0)
                    $this->view->exc = $reading['info'][0];
                
                $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'student', 'Reading Comprehension' => '');
                $this->view->count_reading = Session::get('count_reading');
                $this->view->pageName = 'Reading Comprehension - '.Session::get('fname').' - Ebagrut';
                $this->view->render('reading/index');
            }

            public function grammarPage(){
                $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'student', 'Grammar' => '');
                $this->view->pageName = 'Grammar - '.Session::get('fname').' - Ebagrut';
                $this->view->count_grammar = Session::get('count_grammar');
                $this->view->exc = $this->model->getGrammarExc(Session::get('level'))['info'];
                $this->view->render('grammar/index');
            }

            public function examsPage(){
                $this->view->breadcrumb = array('Home' => URL, Session::get('fname') => URL.'student', 'Exams' => '');
                $this->view->pageName = 'Exams - '.Session::get('fname').' - Ebagrut';
                $examList = $this->model->getExamList()['info'];
                usort($examList, array($this,"sortByModule"));
                $this->view->examList = $examList;
                $this->view->render('exams/index');
            }
            private function sortByModule($a, $b) {
                if ($a == $b)
                    return 0;
                return strcmp($a['module'], $b['module']);
            }
        }
