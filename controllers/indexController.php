<?php

class Index extends Controller{
    
    function __construct() {
        parent::__construct();
        Session::init();
        $logged = Session::get('loggedin');
        $permission = Session::get('permission');
        //redirect according to session/permission
        if($logged == 'true')
        {
            if(($permission == 'root' ) || ($permission == 'admin'))
                header('location:'.URL.'dashboard');
            if($permission == 'default')
                header('location:'.URL.'student');
        }
    }
    //render index page
    function Index(){
        $this->view->pageName = 'Home - Ebagrut';
        $this->view->render('index/index',true);
    }
}