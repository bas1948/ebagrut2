<?php
class View {

    function __construct(){//database connection available here only for Note System and note counting.
        $this->db = new Database(DB_TYPE,DB_HOST,DB_NAME,DB_USER,DB_PASS);
    }

    public function render($name, $noInclude = false)
    {
        if($noInclude == true)
        {
            include 'views/'.$name.'.php';
        }
        else
        {
            include 'views/header.php';
            include 'views/'.$name.'.php';
            include 'views/footer.php';
        }
    }

    public function StudMsgCount($id){
        return $this->db->StudMsgCount($id)['info'][0]['count(*)'];
    }

    public function AdminMsgCount($id){
        return $this->db->AdminMsgCount($id)['info'][0]['count(*)'];
    }
}