<?php
//Session class, currently used to handle all kinds of session related functions.
class Session {
    
    public static function init(){
        @session_start();
    }
    
    public static function set($data = array()){
        foreach ($data as $key => $value) {
            $_SESSION[$key] = $value;
        }
    }
    
    public static function get($key){
        if(isset($_SESSION[$key]))
            return $_SESSION[$key];
    }
    
    public static function destroy(){
        //unset($_SESSION);
        session_destroy();
    }
}