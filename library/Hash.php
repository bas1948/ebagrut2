<?php

class Hash
{
    /**
    /*class for future expanding of password security and hashing algorithms, currently used for creating hash with a predefined salt
    /*@param string $algo The Algorithm
    /*@param string $data the data to ecnode
    /*#param string the salt (DO NOT CHANGE)
    /*@return string the hashed/salted data
    */
    
    public static function create($algo,$data,$salt){
        $context = hash_init($algo,HASH_HMAC,$salt);
        hash_update($context,$data);
        return hash_final($context);
    }
}