<?php



class Database extends PDO{
    function __construct($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS){
        parent::__construct($DB_TYPE.':host='.$DB_HOST.';dbname='.$DB_NAME,$DB_USER,$DB_PASS);
    }

    /**
    *@param string $sql  an sql string
    *@param array $array parameters to bind
    *@param constant $fetchMode a pdo fetchmode
    *@return mixed
    **/
    public function select($sql, $array = array(), $fetchMode = PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            $sth->bindValue(":$key",$value);
        }
        $sth->execute();
        $data = array();
        $data['info'] = $sth->fetchAll($fetchMode);
        $data['count'] = $sth->rowCount();

        return $data;
    }
    /**
    *@param string $table, name of table to insert into
    *@param string $data an associative array
    **/
    public function insert($table, $data)
    {
        ksort($data);
        
        $fieldNames = implode('`,`', array_keys($data));
        $fieldValues = ':' . implode(', :', array_keys($data));

        $sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key",$value);
        }
        $result = $sth->execute();
        //uncomment error for debugging
        /*if (!$result) {
            print_r($sth->errorInfo());
        }*/
        return $this->lastInsertId($result);
    }

    /**
    *@param string $table, name of table to insert into
    *@param string $data an associative array
    *@param string $where to update data
    **/
    public function update($table,$data,$where)
    {
        ksort($data);

        $fieldDetails = NULL;
        foreach ($data as $key => $value) {
            $fieldDetails .= "`$key` = :$key,";
        }

        $fieldDetails = rtrim($fieldDetails,',');

        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key",$value);
        }

        $result = $sth->execute();
        if (!$result) {
            print_r($sth->errorInfo());
        }
    }

    /**
    *@param string $table
    *@param string $where
    *@param integer $limit
    *@return integer Affected Rows
    */
    public function delete($table, $where,$limit = 1){
        return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
    }

    public function temp($sql){
        $sth = $this->prepare($sql);
        $sth->execute();
        /*print_r($sth->errorInfo());
        die;*/
        return $sth->fetchAll();
    }

    public function StudMsgCount($id){
        return $this->select("SELECT count(*) FROM stud_notes WHERE studentID = $id AND is_read = '0' AND NOT sentBy = $id");
    }

    public function AdminMsgCount($id)
    {
        return $this->select("SELECT count(*) FROM stud_notes WHERE teacherID = $id AND is_read = '0' AND NOT sentBy = $id");
    }

}