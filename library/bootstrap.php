<?php
class Bootstrap {
    private $_url = null;
    private $_controller = null;
    function __construct(){}

    public function init(){
        //sets the protected $_url
        $this->_getUrl();

        if($this->_url[0] == "policy")
        {
            View::render("policy",true);
            exit;
        }
        //check if empty, go to homepage(load default controller)
        if(empty($this->_url[0]))
        {
            $this->_loadDefaultController();
            return false;
        }
        //check if controller exists, load it (url)
        $this->_loadController();
        //load methods
        $this->_callControllerMethod();
    }

    private function _getUrl(){
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url,'/');
        $url = filter_var($url,FILTER_SANITIZE_URL);
        $this->_url = explode('/',$url);
    }

    private function _loadDefaultController(){
        require 'controllers/indexController.php';
        $this->_controller = new Index();
        $this->_controller->Index();
    }

    private function _loadController(){
        //build string for controller $file
        $file = 'controllers/'.$this->_url[0].'Controller.php';
        //if controller was found, require it, else: go to error function
        if (file_exists($file)) 
        {
            require_once $file;
            $this->_controller = new $this->_url[0];
            $this->_controller->loadModel($this->_url[0]);
        } 
        else
        {
            $this->error();
            return false;
        }
        //get controller name and load its Model
    }

    private function _callControllerMethod(){
        //start calling methods
        $lenght = count($this->_url);
        if($lenght > 1) 
        {

            if (!method_exists($this->_controller, $this->_url[1])) 
            {
                $this->error();
                exit();
            }
        }
        switch ($lenght) {
            case 1://call controller, index
            $this->_controller->index();
            break;
            
            case 2://call controller method(function)
            $this->_controller->{$this->_url[1]}();
            break;

            case 3://call controller method(function) with 1 parameter
            $this->_controller->{$this->_url[1]}($this->_url[2]);
            break;
            case 4://call controller method(function) with 1 parameter
            $this->_controller->{$this->_url[1]}($this->_url[2],$this->_url[3]);
            break;

            default://call error
            $this->error();
        }
    }

    //start error handling, error controller
    private function error() {
        require 'controllers/errorController.php';
        $this->_controller = new Error();
        $this->_controller->index();
        exit();
    }
    //end error handling
}