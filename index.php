<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
    //use autoloader
require_once 'config/paths.php';
require_once 'config/database.php';
require_once 'config/constants.php';

function __autoload($class){
	require_once LIBS.$class.".php";
}

$bootstrap = new bootstrap();
$bootstrap->init();