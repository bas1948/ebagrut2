<main>
    <div class="container-fluid">
        <?php
    //counter for row, used to determine if this row has background color or not.
        $i=0;
        $j=1;
        if(empty($this->exc) || $this->count_grammar >= 2) { ?>
            <div class="row bgColor" style="font-size: 20px;">
                <p class="col-md-offset-2">No Grammar Exercises for you now, Come back later!</p>
                <p class="col-md-offset-2">You can check out other exercises or previous Bagrut exams.</p>
            </div>
            <div class="row bgColor">
                <div class="container-fluid">
                    <!-- Reading Comprehension -->
                    <div class="col-xm-2 col-md-2 col-md-offset-4 cardContainer">
                        <?php /*Include Grammar Card*/include_once './views/includes/cards/readingCard.php'; ?>
                    </div>
                    <div class="col-xm-2 col-md-2 cardContainer">
                        <?php /*Include exams Card*/include_once './views/includes/cards/examsCard.php'; ?>
                    </div>
                </div>
            </div>
            <?php }
            else { //Getting data from MYSQL, sorting it and displaying it to user ?>
                <form action="<?php echo URL; ?>student/GrammarScore" method="post" class="form-inline">
                        <?php foreach($this->exc as $k=>$v)//loop for database fetching
                        {
                            $ans_array = array('answer1' => $v['answer1'], 'answer2' => $v['answer2'],'answer3' => $v['answer3'], 'correct_answer' => $v['correct_answer']);
                            $shuffle = new Controller();
                            $ans_array = $shuffle->shuffle_assoc($ans_array);
                            ?>
                            <div class="row <?php if($i == 0){
                                echo 'bgColor';
                                $i++;
                            } 
                            else
                                if($i==1)
                                    $i--; ?>">
                                <div class="col-md-12 col-md-offset-3">
                                    <p>
                                        <?php echo $j.') '.$v['question']; 
                                        $j++;
                                        ?>
                                    </p>
                                    <?php foreach ($ans_array as $key => $value) { ?>
                                        <div class="radio form-group col-md-2">
                                            <label>
                                                <input type="radio" name="exc<?php echo $v['excID']; ?>" value="<?php echo $key; ?>" required>
                                                <?php echo $value ?>
                                            </label>
                                        </div>
                                        <?php }//end inner foreach ?>
                                    </div>
                                </div>

                                <?php } //end main foreach?>
                                <div class="container-fluid">
                                    <div class="row">
                                        <input type="submit" value="Submit Answers" class="Button2 col-md-offset-4">
                                    </div>
                                </div>
                            </form>
                            <?php } //end else?>
                        </div>
                    </main>