<footer class="content-info container-fluid" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                &copy; 2016 Basel Shlewett ALL RIGHTS RESERVED. 
                <br>
                <a href="<?php echo URL;?>policy">Privacy Policy</a>
            </div>
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<?php
if(isset($this->js))
{
    foreach($this->js as $js)
    {
        echo '<script type="text/javascript" src="'.URL.'public/'.$js.'"></script>';
    }
}
?>

</body>


</html>
