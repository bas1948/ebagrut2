<p>	
	Copyright <strong>2016 Ebagrut, Basel Shlewett</strong> Licensed under the
	Educational Community License, Version 2.0 (the "License"); you <strong>may
	not</strong> use this file except in compliance with the License. You may
	obtain a copy of the License at
</p>

<p><a href="https://opensource.org/licenses/ECL-2.0"><strong>https://opensource.org/licenses/ECL-2.0</strong></a></p>

<p>
	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an <strong>"AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND</strong>, either express
	or implied. See the License for the specific language governing
	permissions and limitations under the License.
</p>