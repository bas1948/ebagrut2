<main>
	<div class="container-fluid bgColor">
		<div class="row">
			<div class="md-col-6 col-md-offset-3">
				<p>OOOOPS! Looks like you encoutered a 404 ERROR.</p>

				<p>We have dispatched highly trained monkies to fix the error, if you happen to see them please DO NOT FEED THEM!</p>
				
				<p>Meanwhile you can <a href="<?php echo URL;?>"><strong>click here</strong></a> to return to your home page.</p>
			</div>
		</div>
	</div>
</main>