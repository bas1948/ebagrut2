<main>
	<div class="container-fluid bgColor">
		<div class="row">
			<div class="md-col-6 col-md-offset-3">
				<p>OOOOPS! Looks like you were trying to access a page higher than your level of clearance.</p>

				<p>Please refrain from trying to do so, otherwise your account will be suspended!!</p>
				
				<p>Meanwhile you can <a href="<?php echo URL;?>"><strong>click here</strong></a> to return to your home page.</p>
			</div>
		</div>
	</div>
</main>