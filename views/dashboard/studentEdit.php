   <div class="container-fluid bgColor">
    <div class="row col-md-6 col-md-offset-3">
        <table id="userTable" class="table table-responsive table-bordered table-hover table-striped">
            <tr>
                <td>ID</td>
                <td>First Name</td>
                <td>Last Name</td>
                <td>Progress</td>
                <td>Level</td>
            </tr>
            <?php
            foreach($this->SingleUser as $v)
                { ?>
                    <tr>
                        <td>
                            <?php echo $v['user_id']; ?>
                        </td>
                        <td>
                            <?php echo $v['first_name']; ?>
                        </td>
                        <td>
                            <?php echo $v['last_name']; ?>
                        </td>
                        <td>
                            <?php echo $v['progress']; ?>
                        </td>
                        <td>
                            <?php echo $v['level']; ?>
                        </td>
                    </tr>
                    <?php   } ?>
                </table>
            </div>
        </div>

        <div class="container-fluid bgColor">
            <div class="row col-md-12">
                <form action="<?php echo URL; ?>dashboard/editProgress/<?php echo $v['user_id']; ?>" method="post" class="form-horizontal col-md-offset-5">
                    <div class="form-group">
                        <div class="col-md-3 col-xs-2 ">
                            <input name="progress" value="<?php echo $v['progress']; ?>" class="form-control input-md" type="number" min="0" max="100" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-3 col-xs-2 ">
                            <select name="level" id="" class="form-control">
                                <option value="" required>Select Level</option>
                                <option value="3" <?php if($v['level'] == 3) echo 'selected'; ?>>3</option>
                                <option value="4" <?php if($v['level'] == 4) echo 'selected'; ?>>4</option>
                                <option value="5" <?php if($v['level'] == 5) echo 'selected'; ?>>5</option>
                            </select>
                        </div>
                    </div>
                    <input type="submit" value="Save" class="col-md-2 col-xs-2 Button2">
                </form>
            </div>
        </div>