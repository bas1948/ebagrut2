<main>
    <div class="container-fluid bgColor">
        <h2 class="col-md-offset-4">Add Grammar Exercise</h2>
        <div class="row col-md-8 col-md-offset-2 col-xs-11 col-sm-3">
            <form class="form-horizontal" action="<?php echo URL; ?>dashboard/readingAdd" method="post">
                <div class="form-group">
                    <div class="col-md-2 col-md-offset-4 col-xs-12">
                        <select name="level" class="form-control" required>
                            <option value="">Select level</option>
                            <option value="sorting">Sorting</option>
                            <option value="3" >3</option>
                            <option value="4" >4</option>
                            <option value="5" >5</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-2 col-xs-12">
                        <label for="article">Article</label>
                        <textarea id="ReadingArticle" name="article" class="form-control input-md" required><p>Write Article here..</p><p>PS: Remember to delete me first :)</p></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-2 col-xs-12">
                    <label for="question1">First Question</label>
                        <textarea name="question1" class="ckeditor form-control input-md" required><p>Write Question here..</p><p>PS: Remember to delete me first :)</p></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-2 col-xs-12">
                        <label for="question2">Second Question</label>
                        <textarea name="question2" class="ckeditor form-control input-md" required><p>Write Question here..</p><p>PS: Remember to delete me first :)</p></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-2 col-xs-12">
                        <label for="question3">Third Question</label>
                        <textarea name="question3" class="ckeditor form-control input-md" required><p>Write Question here..</p><p>PS: Remember to delete me first :)</p></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-2 col-xs-12">
                        <label for="question4">Fourth Question</label>
                        <textarea  name="question4" class="ckeditor form-control input-md" required><p>Write Question here..</p><p>PS: Remember to delete me first :)</p></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-2 col-xs-12">
                        <label for="question5">Fifth Question</label>
                        <textarea name="question5" class="ckeditor form-control input-md" required><p>Write Question here..</p><p>PS: Remember to delete me first :)</p></textarea>
                    </div>
                </div>
                <input type="submit" value="Add Exercise" class="col-md-2 col-md-offset-4 col-xs-6 Button2 btn">
            </form>
        </div>
    </div>
</main>


<script type="text/javascript">
    CKEDITOR.replace('ReadingArticle',{
        language: 'en',
        height: '400px',
        width: '700px'
    });
</script>