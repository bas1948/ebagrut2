<main>
	<div class="container-fluid bgColor">

		<div class="row col-md-8 col-md-offset-2 col-xs-11 col-sm-3">
			<?php foreach ($this->excInfo as $key => $value) { ?>

				<form class="form-horizontal" action="<?php echo URL; ?>dashboard/grammarEditSave/<?php echo $value['excID']; ?>" method="post">
					<div class="form-group">
						<div class="col-md-2 col-md-offset-4 col-xs-12">
							<select name="level" class="form-control" required>
								<option value="">Select level</option>
								<option value="sorting" <?php if ($value['level'] == 'sorting') echo "selected"; ?>>Sorting</option>
								<option value="3" <?php if ($value['level'] == '3') echo "selected"; ?>>3</option>
								<option value="4" <?php if ($value['level'] == '4') echo "selected"; ?>>4</option>
								<option value="5" <?php if ($value['level'] == '5') echo "selected"; ?>>5</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-2 col-xs-12">
							<!--input id="textinput" name="question" value="" class="form-control input-md" type="text" required-->
							<textarea id="GrammarQ" name="question" class="form-control input-md" required><?php echo $value['question']; ?></p></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-2 col-xs-12">
							<input id="textinput" name="answer1" value="<?php echo $value['answer1']; ?>" class="form-control input-md" type="text" required>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-2 col-xs-12">
							<input id="textinput" name="answer2" value="<?php echo $value['answer2']; ?>" class="form-control input-md" type="text" required>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-2 col-xs-12">
							<input id="textinput" name="answer3" value="<?php echo $value['answer3']; ?>" class="form-control input-md" type="text" required>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-2 col-xs-12">
							<input id="textinput" name="correct_answer" value="<?php echo $value['correct_answer']; ?>" class="form-control input-md" type="text" required>
						</div>
					</div>
					<input type="submit" value="Save Changes" class="col-md-2 col-md-offset-4 col-xs-6 Button2 btn">
				</form>

				<?php } ?>
			</div>
		</div>
	</div>
</main>


<script type="text/javascript">
	CKEDITOR.replace('GrammarQ',{
		language: 'en',
		height: '100px'
	});
</script>