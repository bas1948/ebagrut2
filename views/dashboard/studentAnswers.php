<main>
	<div class="container-fluid bgColor">
		<div class="row">
			<div class="col-md-6 text-center">
				<?php //print_r($this->studAnswers); ?>

				<u><h4><?php echo $this->studAnswers['first_name']." ".$this->studAnswers['last_name']; ?> Answers:</h4></u>


				<?php for ($i=1; $i<6; $i++) { ?>
					<u><h5>Question <?php echo $i; ?>:</h5></u>
					
					<div>
						<strong><?php echo $this->studAnswers['question'.$i]; ?></strong>
					</div>
					
					<textarea cols="60" rows="4" readonly><?php echo $this->studAnswers['answer'.$i]; ?></textarea>
					
					<?php } ?>
				</div>
				<div class="col-md-6 hidden-xs" style="height: 150px;"></div>
				<div class="col-md-6 text-center">
					<h3>Answer check form</h3>
					<form action="<?php echo URL; ?>dashboard/readingScore/<?php echo $this->studAnswers['studentID']; ?>/<?php echo $this->studAnswers['excID']; ?>" method="post" class="form-horizontal">
						<?php for ($i=1; $i<6 ; $i++) { ?>
							<div class="row">
								<div class="radio form-group">
									<h4>Question <?php echo $i; ?></h4>
									<label>
										<input type="radio" name="question<?php echo $i;?>" value="correct" required>
										Correct
									</label>
									<label>
										<input type="radio" name="question<?php echo $i;?>" value="wrong" required>
										Wrong
									</label>
								</div>
							</div>
							<hr>
							<?php } ?>
							<div class="row">
								<input type="submit" value="Submit Answers" class="Button2">
							</div>
					</form>
				</div>
			</div>
		</div>
	</main>

