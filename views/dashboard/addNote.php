<main>
	<div class="row bgColor">
		<div class="container-fluid col-md-6 col-md-offset-3">
			<form action="<?php echo URL; ?>dashboard/addNote/<?php echo $this->userID; ?>" method="post" class="form-horizontal"> 
				<div class="form-group">
					<input name="note_title" type="text" placeholder="Note Title"  required>
				</div>
				<div class="form-group">
					<textarea name="note_content" id="Note" cols="50" rows="10"></textarea>
				</div>
				<input type="submit" value="Send Note" class="Button2">
			</form>
		</div>
	</div>
</main>

<script type="text/javascript">
CKEDITOR.replace('Note',{
		language: 'en',
		height: '400px'
	});
</script>