<main>
    <div class="container-fluid bgColor">
        <div class="row col-md-6 col-md-offset-3">
            <?php if(isset($this->status)) { ?>
                <div class="alert alert-success col-offset-2 col-md-4">
                    <?php echo 'Note Added Successfuly!'; ?>
                </div>
                <?php } ?>
                <table id="userTable" class="table table-responsive table-bordered table-hover table-striped">
                    <tr>
                        <td>Full Name</td>
                        <td>Email</td>
                        <td>Level</td>
                        <td>Progress</td>
                        <td>Options</td>
                    </tr>
                    <?php  foreach($this->Group as $k=>$v)
                    {  ?>
                        <tr>
                            <td>
                                <?php echo $v['first_name']." ".$v['last_name']; ?>
                            </td>
                            <td>
                                <?php echo $v['email']; ?>
                            </td>
                            <td>
                            <?php echo $v['level']; ?>
                            </td>
                            <td>
                                <?php if($v['permission'] == 'admin' || $v['permission'] == 'root') echo "-"; else echo $v['progress']; ?>
                            </td>
                            <td>
                                <?php if($v['permission'] == "default"){ ?>
                                <a href="<?php echo URL; ?>dashboard/studentNote/<?php echo $v['user_id']; ?>">Add Note -</a>
                                <a href="<?php echo URL; ?>dashboard/studentEdit/<?php echo $v['user_id']; ?>">Edit Progress</a>
                                <a href="<?php echo URL; ?>dashboard/studentAnswers/<?php echo $v['user_id']; ?>"> - Check Answers</a>
                                <?php } else
                                echo "-"?>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </main>