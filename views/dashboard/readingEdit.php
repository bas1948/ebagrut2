<main>
   <div class="container-fluid">
    <div class="row bgColor">
        <form class="form-horizontal col-md-2 col-md-offset-5" action="<?php echo URL; ?>dashboard/readingEdit" method="post">
            <div class="form-group">
                <div class="col-md-8">
                    <select name="readingLevel" class="form-control col-xs-12">
                        <option value="">Select level</option>
                        <option value="sorting">Sorting</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>
            <div class="col-md-8 col-xs-8">
                <input type="submit" value="Show Level" class="col-md-12 col-xs-12 Button2 btn">
            </div>
            <div class="col-md-8 col-xs-8" style="margin-top: 3px;">
                <a href="<?php echo URL; ?>dashboard/readingExcAdd" class="col-md-12 col-xs-12 Button2 btn">Add Exercise</a>
            </div>
        </form>
    </div>
</div>
<?php if(isset($this->readingTable))
{ ?>
    <div class="container-fluid bgColor">
        <div class="row col-md-6 col-md-offset-3 col-xs-11 col-sm-3">
            <table id="userTable" class="table table-responsive table-bordered table-hover table-striped text-center">
                <tr>
                    <td>Question</td>
                    <td>Level</td>
                    <td>Option</td>
                </tr>
                <?php
                foreach($this->readingTable as $k=>$v)
                    {  ?>
                        <tr>
                            <td>
                                <?php echo substr($v['article'], 0,120); ?>
                            </td>
                            <td>
                                <?php echo $v['level']; ?>
                            </td>
                            <td>
                                <a href="<?php echo URL; ?>dashboard/readingExcEdit/<?php echo $v['excID']; ?>">View - </a>
                                <a href="<?php echo URL; ?>dashboard/readingExcDelete/<?php echo $v['excID']; ?>">Delete</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
            <?php   }?>
        </main>