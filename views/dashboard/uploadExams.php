<main>
	<div class="container-fluid bgColor">
		<div class="row">
			<div class="col-md-2 col-md-offset-5">
				<?php if(!empty($this->error)){?>
					<div class="alert alert-danger">
						<?php echo $this->error; ?>
					</div>
					<?php } ?>
					<form class="form-horizontal" action="<?php echo URL;?>dashboard/uploadExam" enctype="multipart/form-data" method="post">
						<div class="form-group">
							<div class="col-md-12 col-xs-12">
								<input type="file" name="exam" class="file Button col-md-11" required>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 col-xs-12">
								<select name="module" class="form-control" required>
									<option value="">Module</option>
									<?php $module_arr = array("A","B","C","D","E","F","G");
									foreach ($module_arr as $value) { ?>
										<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<!-- Text input-->
							<div class="form-group">
								<div class="col-md-12 col-xs-12">
									<select name="term" class="form-control" required>
										<option value="">Summer/Winter</option>
										<?php $term_arr = array("Summer","Winter");
										foreach ($term_arr as $value) { ?>
											<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<h5>Date:</h5>
								<div class="form-group" style="margin-left: 2px;">
									<div class="col-md-3 col-xs-3" id="DateInputDiv">
										<select name="day" class="form-control" id="DateInput" required>
											<option value="">Day</option>
											<?php 
											for ($i=1; $i < 32; $i++) { ?> 
												<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
												<?php }
												?>
											</select>
										</div>                                    
										<div class="col-md-3 col-xs-3" id="DateInputDiv">
											<select name="month" class="form-control" id="DateInput" required>
												<option value="">Month</option>
												<?php 
												for ($i=1; $i < 13; $i++) { ?> 
													<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
													<?php }
													?>
												</select>
											</div>
											<div class="col-md-3 col-xs-3" id="DateInputDiv">
												<select name="year" class="form-control" id="DateInput" required>
													<option value="">Year</option>
													<?php 
													for ($i=date('Y'); $i > 1999; $i--) { ?> 
														<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
														<?php }
														?>
													</select>
												</div>
											</div>
											<input type="submit" value="Upload" class="col-md-12 col-xs-12 Button2 btn">
										</form>
									</div>
								</div>
							</div>
							<div class="container-fluid bgColor">
								<div class="row col-md-6 col-md-offset-3  col-sm-3">
									<table id="userTable" class="table table-responsive table-bordered table-hover table-striped text-center">
										<tr>
											<td>Module</td>
											<td>Date</td>
											<td>Term</td>
											<td>Download Link</td>
										</tr>
										<?php
										foreach($this->examList as $k=>$v)
											{  ?>
												<tr>
													<td>
														<?php echo $v['module']; ?>
													</td>
													<td>
														<?php echo $v['date']; ?>
													</td>
													<td>
														<?php echo $v['term']; ?>
													</td>
													<td>
														<a href="<?php echo URL.$v['file_path']; ?>" target="_blank">Download</a>
													</td>
												</tr>

												<?php } ?>
											</table>
										</div>
									</div>
								</main>

								<pre>
									<?php 
									function sortByOrder($a, $b) {
										if ($a == $b)
											return 0;
										return ($a['module'] < $b['module']) ? -1 : 1;
									}
									usort($this->examList, "sortByOrder");
									print_r($this->examList); 

									?>
								</pre>