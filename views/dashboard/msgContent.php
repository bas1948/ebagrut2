<main>
	<div class="container-fluid bgColor">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<h4><?php echo $this->content['title']; ?></h4>
				<p style="font-size: 11px; color: grey;">Sent <?php echo $this->content['date_added']; ?> BY: <? echo $this->content['first_name']." ".$this->content['last_name']; ?></p>
				<div class="msgBody">
					<?php echo $this->content['content']; ?>
				</div>
				<br>
				<div class="col-md-4 col-md-offset-2">
					<a href="<?php echo URL; ?>dashboard/SendNote/<?php echo $this->content['studentID']; ?>" class="Button2">Reply</a>
				</div>
			</div>
		</div>
	</div>
</main>