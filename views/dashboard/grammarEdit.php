<main>
   <div class="container-fluid">
    <div class="row bgColor">
        <form class="form-horizontal col-md-2 col-md-offset-5" action="<?php echo URL; ?>dashboard/grammarEdit" method="post">
            <div class="form-group">
                <div class="col-md-8">
                    <select name="GrammarLevel" class="form-control">
                        <option value="">Select level</option>
                        <option value="sorting">Sorting</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>
            <input type="submit" value="Show Level" class="col-md-8 col-xs-12 Button2 btn">
        </form>
    </div>
</div>
<?php if(isset($this->grammarTable) && !empty($this->grammarTable))
{ ?>
    <div class="container-fluid bgColor">
        <div class="row col-md-6 col-md-offset-3 col-xs-11 col-sm-3">
            <table id="userTable" class="table table-responsive table-bordered table-hover table-striped text-center">
                <tr>
                    <td>Question</td>
                    <td>Level</td>
                    <td>Option</td>
                </tr>
                <?php
                foreach($this->grammarTable as $k=>$v)
                    {  ?>
                        <tr>
                            <td>
                                <?php echo $v['question']; ?>
                            </td>
                            <td>
                                <?php echo $v['level']; ?>
                            </td>
                            <td>
                                <a href="<?php echo URL; ?>dashboard/grammarExcEdit/<?php echo $v['excID']; ?>">View - </a>
                                <a href="<?php echo URL; ?>dashboard/grammarExcDelete/<?php echo $v['excID']; ?>">Delete</a>
                            </td>
                        </tr>
                        <?php } ?>

                    </table>
                </div>
            </div>
            <?php } ?>

            <div class="container-fluid bgColor">
                <h2 class="col-md-offset-4">Add Grammar Exercise</h2>
                <div class="row col-md-8 col-md-offset-2 col-xs-11 col-sm-3">
                    <form class="form-horizontal" action="<?php echo URL; ?>dashboard/grammarAdd" method="post">
                        <div class="form-group">
                            <div class="col-md-2 col-md-offset-4 col-xs-12">
                                <select name="level" class="form-control" required>
                                    <option value="">Select level</option>
                                    <option value="sorting">Sorting</option>
                                    <option value="3" >3</option>
                                    <option value="4" >4</option>
                                    <option value="5" >5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2 col-xs-12">
                                <!--input id="textinput" name="question" placeholder="Question" class="form-control input-md" type="text" required-->
                                <textarea id="GrammarQ" name="question" class="form-control input-md" required><p>Write Question here..</p><p>PS: Remember to delete me first :)</p></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2 col-xs-12">
                                <input id="textinput" name="answer1" placeholder="First Answer" class="form-control input-md" type="text" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2 col-xs-12">
                                <input id="textinput" name="answer2" placeholder="Second Answer" class="form-control input-md" type="text" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2 col-xs-12">
                                <input id="textinput" name="answer3" placeholder="Third Answer" class="form-control input-md" type="text" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2 col-xs-12">
                                <input id="textinput" name="correct_answer" placeholder="Correct Answer" class="form-control input-md" type="text" required>
                            </div>
                        </div>
                        <input type="submit" value="Add Exercise" class="col-md-2 col-md-offset-4 col-xs-6 Button2 btn">
                    </form>
                </div>
            </div>
        </main>


        <script type="text/javascript">
            CKEDITOR.replace('GrammarQ',{
                language: 'en',
                height: '100px'
            });
        </script>