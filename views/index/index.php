<!DOCTYPE html>
<?php Session::init(); ?>
<html lang="en">
<head>
    <title>Home</title>
       <link rel="stylesheet" href="<?php echo URL;?>public/css/main.css" type="text/css">
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta charset="utf-8">
   </head>
   <body class="LandingBody">
<main>
    <div class="container-fluid">
        <div class="row bgIMG">
            <div class="col-xm-2 col-md-4 col-md-offset-4 cardContainer">
                <div class="card" id="signup">
                    <div class="front specialFront text-center ">
                        <h1>English Bagrut Coming Up?</h1>
                        <p> We'll help you prepare.. for free!</p>
                        <div id="Buttons">
                            <button class="Button2 FLIP">Sign up</button>
                            <a href="<?php echo URL;?>login" class="Button">Log in</a>
                        </div>
                    </div>
                    <div class="back specialBack" id="back">
                        <div class="content">
                            <h4>Sign up and start studying in seconds.</h4>
                                <?php /*INCLUDE REGISTRATION FORM*/ include_once './views/register/registerForm.php'; ?>
                                <button class="Button2 FLIP btn col-md-12 col-xs-12">Back</button>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row bgIMG3">
            <div class="col-sm-8 col-sm-offset-2">
                <h2>Why English?</h2>
                <p>You might want to ask why I chose precisely English Bagruts? Is it the language? Because it's easy to learn? well maybe it's a blend of both in addition to many reasons!</p>
                <p>Nowadays we live in the age of Globalization.. meaning the world is beggining to feel smaller with each passing day, and what's a better and easier language to communicate with each other besides English?</p>
                <p>Are you aiming to work in a Worldwide Company? well, without basic knowledge of English you probably won't get far. Emails, meetings, information, all of those are shared in a global Language, which is English.</p>
                <p>And yes maybe I'm a little soft for the Language.</p>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row bgIMG2">
            <div class="col-sm-8 col-sm-offset-2 textContainer">
                <h2>Who Are We?</h2>
                <p>Currently I am an ambitious 'practical software engineering' student aiming to finish my studies and dive into the real world, where problems don't wait around for solutions.</p>
                <p>Choosing software engineering wasn't a hard decision to make, I have always been curious about how different websites and applications were written, how do they work and why do they do what they do.</p>
                <p>As my passion for developement grew, I became instantly interested in web developement as how I see it.. that's our future. Day after day, out lifes are becoming dependent on various websites that offer Search enginges, and Social networking, like Facebook and Twitter.</p>
                <p>This website serves as a final project for my college. hope you enjoy you visit.</p>
                <h3>Why Us?</h3>
                <p>You certainly asked what makes us special and we're different from our competitors?</p>
                <p>While you might find multiple websites offering you lectures and previous examples of Bagrut exams, we promise you a more direct way of preparing yourself for the exam.</p>
                <p>Our features include full interactions with different teachers from all over the country, and original content you won't experience in any other place, we also offer you an array of previous Bagrut exams.</p>
                <p>Don't believe us? Well sign up and take a look!</p>
            </div>
        </div>
    </div>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo URL;?>public/js/flip.js"></script>
    </body>
</html>