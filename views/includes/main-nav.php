<div class="navbar navbar-default col-md-8 col-md-offset-2" role="navigation" style="background-color:#fff;" id="navbar-noBorder">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainMenu">
                <span class="sr-only">Toggle Navigatio</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php echo URL;?>" class="navbar-brand">E-Bagrut</a>
        </div>
        <div class="navbar-collapse collapse" id="mainMenu">
            <ul class="nav navbar-nav navbar-right">
                <?php if(Session::get('loggedin') == true && (Session::get('permission') == 'root' || Session::get('permission') == 'admin'))
                { ?>
                    <li><a href="<?php echo URL;?>dashboard" class="Button2 btn">Dash Board</a></li>
                    <?php    if(Session::get('permission') == 'root')
                    { ?>
                        <li><a href="<?php echo URL;?>user" class="Button text-center">Users</a></li>
                        <?php } ?>
                        <li><a href="<?php echo URL;?>login/logout" class="Button2 btn">Log Out</a></li>
                        <?php }
                        else
                        {
                            if(Session::get('loggedin') == true && Session::get('permission') == 'default')
                                { ?>
                                    <li><a href="<?php echo URL;?>student" class="Button2 btn">Home</a></li>
                                    <li><a href="<?php echo URL;?>login/logout" class="Button2 btn">Log Out</a></li>
                                    <?php }
                                    else
                                        { ?>
                                            <li><a href="<?php echo URL;?>" class="Button2 btn">Home</a></li>
                                            <li><a href="<?php echo URL; ?>register" class="Button2 btn">Sign up</a></li>
                                            <li><a href="<?php echo URL;?>login" class="Button2 btn">Log in</a></li>
                                            <?php }
                                        } ?>
                                    </ul>
                                </div>
                        <?php if(isset($this->name) && isset($this->progress)) {?>
                            <div class="container-fluid">
                                <div class="row col-md-3 col-xs-12 col-md-offset-4">
                                    <div class="Button col-md-10 text-center">
                                        <?php echo 'Welcome back '.$this->name.'!<br>Your level is: '.$this->level; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-6 col-xs-12 col-md-offset-3">
                                <?php $this->progress = intval($this->progress); 
                                if($this->progress > 0)
                                    { ?>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $this->progress; ?>" aria-valuemin="0" aria-valuemax="100" style="background-color: #34b3a0;width:<?php echo $this->progress; ?>%">
                                                <span><?php echo $this->progress; ?>% Complete</span>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>