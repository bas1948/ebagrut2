<div class="navbar navbar-default col-md-6 col-md-offset-3" role="navigation"  style="background-color:#D7DBDB;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#subMenu">
                <span class="sr-only">Toggle Admin Navigatio</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Admin Menu</a>
        </div>
        <div class="navbar-collapse collapse" id="subMenu">
            <ul class="nav navbar-nav navbar-right">
                <?php if(Session::get('permission') == 'root')
                { ?>
                    <li><a href="<?php echo URL; ?>user" class="Button2 text-center">Users</a></li>
                    <li><a class="Button2  text-center" href="#">Groups</a></li>
                    <?php } ?>
                    <li><a class="Button2  text-center" href="<?php echo URL; ?>dashboard/inboxTitle">Inbox <?php if ($this->AdminMsgCount(Session::get('user_id')) > 0) { ?> <span class="badge progress-bar-success"><?php echo $this->AdminMsgCount(Session::get('user_id')); ?></span> <?php } ?></a></li>
                    <li><a class="Button2  text-center" href="<?php echo URL; ?>dashboard/outboxTitle">OutBox</a></li>
                    <li><a href="<?php echo URL;?>dashboard" class="Button2  text-center">Dash Board</a></li>
                    <li><a class="Button2  text-center" href="<?php echo URL; ?>dashboard/grammaredit">Grammar</a></li>
                    <li><a class="Button2  text-center" href="<?php echo URL; ?>dashboard/readingedit">Reading</a></li>
                    <li><a class="Button2  text-center" href="<?php echo URL; ?>dashboard/uploadExamsPage">Upload Exams</a></li>
                </ul>
            </div>
        </div>
    </div>