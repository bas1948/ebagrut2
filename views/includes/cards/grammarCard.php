<div class="card" id="grammarCard">
    <div class="front">
        <h3>Grammar Exercises</h3>
        <p>
            A variety of Grammar Exercises to prepare you in the best possible Way
        </p>
        <p>
            Click the Button below for more information and instructions.
        </p>
        <div id="Buttons">
            <button class="Button2 FLIP col-xs-offset-4 col-md-offset-0">Read More..</button>
        </div>
    </div>
    <div class="back" id="back">
        <div class="content">
            <h3>Grammar Instructions</h3>
            <p>
                In this section you'll be presented with 10 grammar questions, you'll be required to pick the correct answer out of 4 possible answers.
            </p>
            <p>
                You'll be rewared X points for each correct answer. This section is required to be answered fully to progress to the next level.
            </p>
            <div id="Buttons">
                <a href="<?php echo URL;?>student/grammarPage">
                    <button class="Button2">Go to Grammar..</button>
                </a>
            </div>
        </div>
    </div>
</div>