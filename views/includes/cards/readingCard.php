<div class="card" id="readingCard">
    <div class="front">
        <h3>Reading Comprehension</h3>
        <p>
            In this siction you'll be presented with an Article and Exercises about it.
        </p>
        <p>
            Click the Button below for more information and instructions.
        </p>
        <div id="Buttons">
            <button class="Button2 FLIP col-xs-offset-4 col-md-offset-0">Read More..</button>
        </div>
    </div>
    <div class="back" id="back">
        <div class="content">
            <h3>Reading Comprehension Instructions</h3>
            <p>
                In this section you'll be presented with an article and questions relating to that artcile, you'll be required to fill the answers in your language, and a teacher will check your answers shortly.
            </p>
            <p>
                You'll be rewared X points for each correct answer. This section is required to be answered fully to progress to the next level.
            </p>
            <div id="Buttons">
                <a href="<?php echo URL;?>student/readingPage">
                    <button class="Button2">Go to Reading Comprehension..</button>
                </a>
            </div>
        </div>
    </div>
</div>