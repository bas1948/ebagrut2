<div class="navbar navbar-default col-md-6 col-md-offset-3" role="navigation"  style="background-color:#D7DBDB;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#subMenu">
                <span class="sr-only">Toggle Student Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Student Menu</a>
        </div>
        <div class="navbar-collapse collapse" id="subMenu">
            <ul class="nav navbar-nav navbar-right">
        <?php if(Session::get('permission') == 'root')
          { ?>
                <li><a href="<?php echo URL; ?>user" class="Button2 text-center">Users</a></li>
                <?php } ?>
                <li><a href="<?php echo URL;?>student/MessageTeacher" class="Button2  text-center">Message Teacher</a></li>
                <li><a href="<?php echo URL;?>student/inboxTitle" class="Button2  text-center">Inbox <?php if ($this->StudMsgCount(Session::get('user_id')) > 0) { ?> <span class="badge progress-bar-success"><?php echo $this->StudMsgCount(Session::get('user_id')); ?></span> <?php } ?></a></li>
                <li><a href="<?php echo URL;?>student/outboxTitle" class="Button2  text-center">OutBox</a></li>
            </ul>
        </div>
    </div>
</div>