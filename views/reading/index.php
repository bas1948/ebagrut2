<main>
    <?php   if(empty($this->exc) || $this->count_reading >= 2) { ?>
        <div class="container-fluid bgColor">
            <div class="row">
                <div class="col-md-12 col-md-offset-4">
                    <div class="row">
                        <p>No Reading Comprehension Exercises for you now, Come back later!</p>
                        <p>You can check out other exercises or previous Bagrut exams.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <?php /*Include Grammar Card*/include_once './views/includes/cards/grammarCard.php'; ?>
                        </div>
                        <div class="col-md-2">
                            <?php /*Include Grammar Card*/include_once './views/includes/cards/examsCard.php'; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php }
        else{ ?>
            <div class="container-fluid bgColor">`
                <form action="<?php echo URL; ?>student/saveAnswers/<?php echo $this->exc['excID']; ?>" class="form-horizontal" method="post">
                    <?php foreach($this->exc as $k=>$v){ 
                        if($k == 'excID' || $k == 'level' || $k == 'article_desc')
                            continue;?>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-3">
                                <div class="container col-md-8 <?php if($k == 'article') echo 'ReadingArticle'; ?>">
                                    <?php  if($k == 'article') echo $v; 
                                    
                                    if($k != 'article') { ?>
                                        <div class="row" style="margin-bottom: 10px;">
                                            <div class="container-fluid">
                                                <p><?php echo $v; ?></p>
                                            </div>
                                        </div>
                                        <textarea name="<?php echo $k; ?>" id="" rows="2" class="col-md-12 ReadingForm" placeholder="Your Answer..." required></textarea>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!-- Seperator -->
                            <div class="container-fluid bgColor">
                                <div class="row">
                                    <hr>
                                </div>
                            </div>

                            <?php } ?>
                            <div class="row">
                                <div class="container col-md-5 col-md-offset-5">
                                    <input type="submit" class="Button2" value="Submit Answers">
                                </div>
                            </div>

                        </form>
                    </div>
                    <?php  } ?>
                </main>
