<div class="container-fluid bgColor">
    <div class="row col-md-6 col-md-offset-3  col-sm-3">
        <table id="userTable" class="table table-responsive table-bordered table-hover table-striped text-center">
            <tr>
                <td>Module</td>
                <td>Date</td>
                <td>Term</td>
                <td>Download Link</td>
            </tr>
            <?php
            foreach($this->examList as $k=>$v)
                {  ?>
                    <tr>
                        <td>
                            <?php echo $v['module']; ?>
                        </td>
                        <td>
                            <?php echo $v['date']; ?>
                        </td>
                        <td>
                            <?php echo $v['term']; ?>
                        </td>
                        <td>
                            <a href="<?php echo URL.$v['file_path']; ?>" target="_blank">Download</a>
                        </td>
                    </tr>

                    <?php } ?>
                </table>
            </div>
        </div>