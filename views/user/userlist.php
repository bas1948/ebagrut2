<div class="container-fluid bgColor">
    <div class="row col-md-11 col-md-offset-5  ">
        <form action="<?php echo URL; ?>user/create" method="post" class="form-horizontal col-xs-9 col-sm-6">
            <!-- Text input-->
            <div class="form-group">
                <div class="col-md-3">
                    <input id="textinput" name="fname" placeholder="First Name" class="form-control input-md" type="text" required>
                </div>
            </div>
            <!-- Text input-->
            <div class="form-group">
                <div class="col-md-3">
                    <input id="textinput" name="lname" placeholder="Last Name" class="form-control input-md" type="text" required>
                </div>
            </div>
            <!-- Text input-->
            <div class="form-group">
                <div class="col-md-3">
                    <input id="textinput" name="email" placeholder="Email" class="form-control input-md" type="email" required>

                </div>
            </div>
            <!-- Password input-->
            <div class="form-group">
                <div class="col-md-3">
                    <input id="passwordinput" name="pword" placeholder="Password" class="form-control input-md" type="password" required>
                </div>
            </div>
            <!-- Password input-->
            <div class="form-group">
                <div class="col-md-3">
                    <input id="passwordinput" name="ver_pword" placeholder="Verify Password" class="form-control input-md" type="password" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <select name="permission" id="" class="form-control">
                        <option value="default">Default User</option>
                        <option value="admin">Admin</option>
                    </select>
                </div>
            </div>
            <input type="submit" value="Add User" class="col-md-2 Button2">
        </form>
    </div>
</div>




<div class="container-fluid bgColor">
    <div class="row col-md-6 col-md-offset-3 col-sm-3">
        <table id="userTable" class="table table-responsive table-bordered table-hover table-striped">
            <tr>
                <td>ID</td>
                <td>First Name</td>
                <td>Last Name</td>
                <td>Email</td>
                <td>D.O.B</td>
                <td>Permission</td>
                <td>Options</td>
            </tr>
            <?php
            foreach($this->userList as $k=>$v)
                {  ?>
                    <tr>
                        <td>
                            <?php echo $v['user_id']; ?>
                        </td>
                        <td>
                            <?php echo $v['first_name']; ?>
                        </td>
                        <td>
                            <?php echo $v['last_name']; ?>
                        </td>
                        <td>
                            <?php echo $v['email']; ?>
                        </td>
                        <td>
                            <?php echo $v['birthDay']; ?>
                        </td>
                        <td>
                            <?php echo $v['permission']; ?>
                        </td>
                        <td>
                            <?php 
                            if($v['permission'] == 'root')
                                echo 'N/A';
                            else {?>
                                <a href="<?php echo URL; ?>user/edit/<?php echo $v['user_id']; ?>">Edit - </a>
                                <a href="<?php echo URL; ?>user/delete/<?php echo $v['user_id']; ?>">Delete</a>

                                <?php   } ?>
                            </td>
                        </tr>

                        <?php } ?>
                    </table>
                </div>
            </div>