
<div class="container-fluid bgColor">
    <div class="row col-md-6 col-md-offset-3">
      <table id="userTable" class="table table-responsive table-bordered table-hover table-striped">
        <tr>
            <td>ID</td>
            <td>First Name</td>
            <td>Last Name</td>
            <td>Email</td>
            <td>D.O.B</td>
            <td>Permission</td>
            <?php if ($this->user[0]['permission'] == 'default') { ?>
                <td>Level</td>
                <?php } ?>
            </tr>
            <?php
            foreach($this->user as $v)
                { ?>
                    <tr>
                        <td>
                            <?php echo $v['user_id']; ?>
                        </td>
                        <td>
                            <?php echo $v['first_name']; ?>
                        </td>
                        <td>
                            <?php echo $v['last_name']; ?>
                        </td>
                        <td>
                            <?php echo $v['email']; ?>
                        </td>
                        <td>
                            <?php echo $v['birthDay']; ?>
                        </td>
                        <td>
                            <?php echo $v['permission']; ?>
                        </td>
                        <?php if($v['permission'] == 'default') { ?>
                            <td>
                                <?php print_r( $v['level']); ?>
                            </td>
                            <?php } ?>
                        </tr>
                        <?php   } ?>
                    </table>
                </div>
            </div>

            <div class="container-fluid bgColor">
                <div class="row col-md-12">
                    <form action="<?php echo URL; ?>user/editSave/<?php echo $v['user_id']; ?>" method="post" class="form-horizontal col-md-offset-5">
                        <!-- Text input-->
                        <div class="form-group">
                            <div class="col-md-3 col-xs-2 ">
                                <label for="fname">First Name:</label>
                                <input id="textinput" name="fname" value="<?php echo $v['first_name']; ?>" class="form-control input-md" type="text" required>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <div class="col-md-3 col-xs-2">
                                <label for="lname">Last Name:</label>
                                <input id="textinput" name="lname" value="<?php echo $v['last_name']; ?>" class="form-control input-md" type="text" required>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <div class="col-md-3 col-xs-2">
                                <label for="email">Email:</label>
                                <input id="textinput" name="email" value="<?php echo $v['email']; ?>" class="form-control input-md" type="email" required>

                            </div>
                        </div>
                        <!-- Password input-->
                        <div class="form-group">
                            <div class="col-md-3 col-xs-2 ">
                                <label for="pword">Password:</label>
                                <input id="passwordinput" name="pword" placeholder="Password" class="form-control input-md" type="password" required>
                            </div>
                        </div>
                        <!-- Password input-->
                        <div class="form-group">
                            <div class="col-md-3 col-xs-2 ">
                                <label for="ver_pword">Verify Password:</label>
                                <input id="passwordinput" name="ver_pword" placeholder="Verify Password" class="form-control input-md" type="password" required>
                            </div>
                        </div>
                        <?php if($v['permission']=='default') { ?>
                            <div class="form-group">
                                <div class="col-md-3 col-xs-2 ">
                                    <label for="level">Level:</label>
                                    <select name="level" id="" class="form-control">
                                        <option value="">Select Level</option>
                                        <option value="3" <?php if($v['level']=='3') echo 'selected';?>>3</option>
                                        <option value="4" <?php if($v['level']=='4') echo 'selected';?>>4</option>
                                        <option value="5" <?php if($v['level']=='5') echo 'selected';?>>5</option>
                                    </select>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="form-group">
                                <div class="col-md-3 col-xs-2 ">
                                    <label for="permission">Permission:</label>
                                    <select name="permission" id="" class="form-control">
                                        <option value="default" <?php if($v['permission']=='default') echo 'selected'; ?>>Default User</option>
                                        <option value="admin" <?php if($v['permission']=='admin') echo 'selected'; ?>>Admin</option>
                                    </select>
                                </div>
                            </div>
                            <input type="submit" value="Save" class="col-md-2 col-xs-2 Button2">
                        </form>
                    </div>
                </div>