<main>
    <div class="container-fluid">
        <div class="row bgColor">
            <div class="container-fluid col-md-2 col-md-offset-5">
                <p style="color:green;">
                    <strong>
                        <?php 
                        if (isset($this->success)) { ?>
                            <div class="alert alert-success">
                                <?php echo $this->success; ?>
                            </div>
                            <?php }
                            ?>
                        </strong>
                    </p>
                </div>
                <form class="form-horizontal" action="<?php echo URL;?>login/run" method="post">
                    <!-- Email-->
                    <div class="form-group">
                        <div class="col-md-4 col-xs-6 col-xs-offset-4">
                            <input name="email" placeholder="Email" class="form-control input-md" type="email" required>

                        </div>
                    </div>
                    <!-- Password -->
                    <div class="form-group">
                        <div class="col-md-4 col-xs-6 col-xs-offset-4">
                            <input name="pword" placeholder="Password" class="form-control input-md" type="password" required>
                        </div>
                    </div>
                    <input type="submit" value="Log in" class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-4 Button2">
                </form>
            </div>
        </div>
    </main>
