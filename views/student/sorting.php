<main>
    <div class="container-fluid bgColor">
        <div class="row">
            <div class="container col-md-5 col-md-offset-3 Button">
                <h4>Welcome
                    <?php echo $this->name; ?></h4>
                    <p> This is a sorting exam, based on your result on this test you'll be assigned to a group and teacher. You are going to be presented with a short article and questions about it, after that you'll need to answer 10 grammar questions.</p>
                    <p> Good Luck!</p>
                </div>
            </div>
        </div>
        <div class="container-fluid bgColor">
            <div class="row">
                <hr>
            </div>
        </div>
        <form action="<?php echo URL;?>student/Sorting" method="post" class="form-inline">
            <?php
        //counter for row, used to determine if this row has background color or not.
            $i=0;
            $j=1;
        //loop for database fetching
            foreach($this->grammar as $k=>$v)
            {
                    //method to shuffle answers array for random radio-buttons sorting.
                $ans_array = array('answer1' => $v['answer1'], 'answer2' => $v['answer2'],'answer3' => $v['answer3'], 'correct_answer' => $v['correct_answer']);
                $shuffle = new Controller();
                $ans_array = $shuffle->shuffle_assoc($ans_array);
                ?>
                <div class="container-fluid">
                    <div class="row <?php if($i == 0){
                        echo 'bgColor';
                        $i++;
                    } 
                    else
                        if($i==1)
                            $i--; ?>">
                        <div class="col-md-9 col-md-offset-3 col-xs-10 col-xs-offset-2">
                            <p>
                                <?php echo $j.') '.$v['question']; 
                                $j++;
                                ?>
                            </p>
                            <?php foreach ($ans_array as $key => $value) { ?>
                                <div class="radio form-group col-md-2">
                                    <label>
                                        <input type="radio" name="exc<?php echo $key; ?>" value="answer1" required>
                                        <?php echo $value; ?>
                                    </label>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row Seperator<?php if($i==1){ echo 'bgColor';} ?>">
                        </div>
                    </div>
                    <?php } ?>
                    <div class="container-fluid col-md-offset-5 col-xs-offset-3">
                        <div class="row">
                            <input type="submit" class="Button2" value="Submit Answers">
                        </div>
                    </div>
                </form>
            </main>