<main>
	<div class="container-fluid bgColor">
		<div class="row">
			<form class="form-horizontal col-md-2 col-md-offset-5" action="<?php echo URL; ?>student/SendTeacher" method="post">
				<div class="form-group col-md-10">
					<select name="teacherID" class="form-control" required>
						<option value="">Select Teacher</option>
						<?php 
						foreach($this->admins as $k => $v) { ?> 
							<option value="<?php echo $v['user_id']; ?>"><?php echo $v['first_name']." ".$v['last_name']; ?></option>
							<?php }
							?>
						</select>
					</div>
					<input type="submit" value="Message Teacher" class="col-md-8 col-xs-12 Button2 btn">
				</form>
			</div>
		</div>
	</main>


