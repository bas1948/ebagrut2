<main>
    <div class="container-fluid bgColor">
        <div class="row">
            <div class="col-xm-2 col-md-2 col-md-offset-3 cardContainer">
                <?php /*Include Grammar Card*/include_once './views/includes/cards/grammarCard.php'; ?>
            </div>

            <!-- Reading Comprehension -->

            <div class="col-xm-2 col-md-2 cardContainer">
                <?php /*Include Grammar Card*/include_once './views/includes/cards/readingCard.php'; ?>
            </div>
            <div class="col-xm-2 col-md-2 cardContainer">
                <?php /*Include Grammar Card*/include_once './views/includes/cards/examsCard.php'; ?>
            </div>
        </div>
    </div>
</main>