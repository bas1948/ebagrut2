<main>
	<div class="container-fluid bgColor">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<h4><?php echo $this->content['title']; ?></h4>
				<p style="font-size: 11px; color: grey;"><strong>Sent</strong> <?php echo $this->content['date_added']; ?> <strong>BY:</strong> <? if($this->content['first_name'] == 'root') echo "E-bagrut"; else echo $this->content['first_name']." ".$this->content['last_name']; ?></p>
				<div class="msgBody">
					<?php echo $this->content['content']; ?>
				</div>
				<br>
				<div class="col-md-4 col-md-offset-2">
					<a href="<?php echo URL; ?>student/SendNote/<?php echo $this->content['teacherID']; ?>" class="Button2">Reply</a>
				</div>
			</div>
		</div>
	</div>
</main>