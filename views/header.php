<!DOCTYPE html>
<?php Session::init(); ?>
<html lang="en">
<head>
    <title><?php if(isset($this->pageName)) echo $this->pageName;
       else echo 'Home'; ?></title>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
       <link rel="stylesheet" href="<?php echo URL;?>public/css/main.css" type="text/css">
       <script src="//cdn.ckeditor.com/4.5.9/basic/ckeditor.js"></script>
       <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
       <meta charset="utf-8">
   </head>
   <body>
       <div class="container-fluid" id="mainNavBar">
        <header>
            <?php include_once './views/includes/main-nav.php'; ?>
        </header>
    </div>
    <div class="container-fluid bgColor">
        <?php if(Session::get('permission') == 'root' || Session::get('permission') == 'admin')
                  include_once './views/includes/navbar-left.php'; 
              elseif(Session::get('permission') == 'default')
                  include_once './views/includes/student-navbar.php'; ?>
        </div>
        <?php if(isset($this->breadcrumb) && count($this->breadcrumb)>1) { ?>
            <div class="container-fluid bgColor">
                <div class="row col-md-offset-3 col-md-6">
                    <?php include_once './views/includes/breadcrumbs.php'; ?>
                </div>
            </div>
            <?php } ?>