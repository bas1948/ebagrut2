<?php if (isset($this->error) && !empty($this->error)) { ?>
    <div class="alert alert-danger">
        <?php echo $this->error; ?>
    </div>
    <?php } ?>
    <form class="form-horizontal" action="<?php echo URL; ?>register/create" method="post">
        <!-- Text input-->
        <div class="form-group">
            <div class="col-md-12 col-xs-12">
                <input id="textinput" name="fname" placeholder="First Name" class="form-control input-md" type="text" required>
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <div class="col-md-12 col-xs-12">
                <input id="textinput" name="lname" placeholder="Last Name" class="form-control input-md" type="text" required>
            </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
            <div class="col-md-12 col-xs-12">
                <input id="textinput" name="email" placeholder="Email" class="form-control input-md" type="email" required>
            </div>
        </div>
        <!-- Password input-->
        <div class="form-group">
            <div class="col-md-12 col-xs-12">
                <input id="passwordinput" name="pword" placeholder="Password" class="form-control input-md" type="password" required>
            </div>
        </div>
        <!-- Password input-->
        <div class="form-group">
            <div class="col-md-12 col-xs-12">
                <input id="passwordinput" name="ver_pword" placeholder="Verify Password" class="form-control input-md" type="password" required>
            </div>
        </div>
        <h5>Birthday:</h5>
        <div class="form-group" style="margin-left: 2px;">
            <div class="col-md-3 col-xs-3" id="DateInputDiv">
                <select name="day" class="form-control" id="DateInput" required>
                    <option value="">Day</option>
                    <?php 
                    for ($i=1; $i < 32; $i++) { ?> 
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php }
                        ?>
                    </select>
                </div>                                    
                <div class="col-md-3 col-xs-3" id="DateInputDiv">
                    <select name="month" class="form-control" id="DateInput" required>
                        <option value="">Month</option>
                        <?php 
                        for ($i=1; $i < 13; $i++) { ?> 
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3 col-xs-3" id="DateInputDiv">
                        <select name="year" class="form-control" id="DateInput" required>
                            <option value="">Year</option>
                            <?php 
                            for ($i=date('Y')-15; $i > 1950; $i--) { ?> 
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>
                    <input type="submit" value="Register" class="col-md-12 col-xs-12 Button2 btn">
                </form>