<main>
        <div class="container-fluid text-center bgColor">
            <div class="row">
                <div class="col-sm-3 col-sm-offset-3">
                    <img src="<?php echo URL;?>public/images/learn.jpg" alt="" id="learn" class="img-responsive">
                </div>
                <div class="col-xm-2 col-md-2 cardContainer">
                    <div class="card">
                        <div class="front">
                            <h2>English Bagrut Coming Up?</h2>
                            <p> We'll help you prepare.. for free!</p>
                            <div id="Buttons">
                                <button class="Button2 FLIP">Sign up</button>
                                <a href="<?php echo URL;?>login" class="Button">Log in</a>
                            </div>
                        </div>
                        <div class="back" id="back">
                            <div class="content">
                                <h4>Sign up and start studying in seconds.</h4>
                                <form class="form-horizontal" action="models/registrationModel.php" method="post">
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <div class="col-md-7 col-xs-6 col-xs-offset-3">
                                            <input id="textinput" name="fname" placeholder="First Name" class="form-control input-md" type="text" required>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <div class="col-md-7 col-xs-6 col-xs-offset-3">
                                            <input id="textinput" name="lname" placeholder="Last Name" class="form-control input-md" type="text" required>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <div class="col-md-7 col-xs-6 col-xs-offset-3">
                                            <input id="textinput" name="email" placeholder="Email" class="form-control input-md" type="email" required>

                                        </div>
                                    </div>
                                    <!-- Password input-->
                                    <div class="form-group">
                                        <div class="col-md-7 col-xs-6 col-xs-offset-3">
                                            <input id="passwordinput" name="passwordinput" placeholder="Password" class="form-control input-md" type="password" required>
                                        </div>
                                    </div>
                                    <!-- Password input-->
                                    <div class="form-group">
                                        <div class="col-md-7 col-xs-6 col-xs-offset-3">
                                            <input id="passwordinput" name="passwordinput" placeholder="Verify Password" class="form-control input-md" type="password" required>
                                        </div>
                                    </div>
                                    <input type="submit" value="Register" class="col-md-7 col-xs-6 col-xs-offset-3 Button2">
                                </form>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-3 textContainer">
                    <h3>Why English?</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta aliquam minima saepe optio eveniet praesentium harum voluptatem quam aspernatur modi dolore itaque consequatur tenetur, atque doloremque, hic tempora quisquam maxime!</p>
                    <p>Odit odio deserunt necessitatibus esse, nihil modi repellat tenetur. Esse fugit facilis ut ipsam voluptas veritatis repudiandae numquam quaerat autem, id cupiditate qui quidem vel placeat animi consectetur voluptatibus vero!</p>
                    <p>Quia tenetur dolorum magni odio laborum adipisci repellat soluta numquam! Perferendis similique ratione, fugit repudiandae doloremque assumenda fuga hic quo, quaerat neque odio. Eos, suscipit. Sequi soluta voluptates quis sunt!</p>
                    <p>Odit odio deserunt necessitatibus esse, nihil modi repellat tenetur. Esse fugit facilis ut ipsam voluptas veritatis repudiandae numquam quaerat autem, id cupiditate qui quidem vel placeat animi consectetur voluptatibus vero!</p>
                    <p>Quia tenetur dolorum magni odio laborum adipisci repellat soluta numquam! Perferendis similique ratione, fugit repudiandae doloremque assumenda fuga hic quo, quaerat neque odio. Eos, suscipit. Sequi soluta voluptates quis sunt!</p>
                </div>
                <div class="col-sm-3">
                    <img src="<?php echo URL;?>public/images/thinking.jpg" alt="" id="think" class="img-responsive">
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row bgColor">
                <div class="col-sm-3 col-sm-offset-3">
                    <img src="<?php echo URL;?>public/images/whyUS.jpg" alt="" id="US" class="img-responsive">
                </div>
                <div class="col-sm-4 textContainer">
                    <h2>Who Are We?</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et nostrum quisquam odio temporibus numquam ex pariatur in, eum culpa dolores. Placeat cupiditate saepe minus earum quidem necessitatibus libero, tempore similique.</p>
                    <p>Eveniet deleniti eligendi nesciunt, velit accusantium. Molestiae nihil, sapiente minima qui quia quo laboriosam eaque esse eveniet, accusantium. Ratione veritatis omnis culpa. Cum optio porro omnis, deleniti veniam esse quas!</p>
                    <h3>Why Us?</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta aliquam minima saepe optio eveniet praesentium harum voluptatem quam aspernatur modi dolore itaque consequatur tenetur, atque doloremque, hic tempora quisquam maxime!</p>
                    <p>Odit odio deserunt necessitatibus esse, nihil modi repellat tenetur. Esse fugit facilis ut ipsam voluptas veritatis repudiandae numquam quaerat autem, id cupiditate qui quidem vel placeat animi consectetur voluptatibus vero!</p>
                    <p>Quia tenetur dolorum magni odio laborum adipisci repellat soluta numquam! Perferendis similique ratione, fugit repudiandae doloremque assumenda fuga hic quo, quaerat neque odio. Eos, suscipit. Sequi soluta voluptates quis sunt!</p>
                </div>
            </div>
        </div>
    </main>