'use strict';
$(function () {
    $.get('dashboard/xhrGetListings', function (o) {
        var i;
        for (i = 0; i < o.length; i++) {
            $('#listInsert').append('<div>' + o[i].text + '<a class="del" rel="' + o[i].id + '" href="#">X</a></div>');
        }
        $('div').on('click', '.del', function () {
            var id = $(this).attr('rel'),
            delItem = $(this);
            $.post('dashboard/xhrDeleteListing', {'id': id}, function (o) {
                delItem.parent().remove();
            });
            return false;
        });
    }, 'json');
    $('#rndInsert').submit(function () {
        var url = $(this).attr('action'),
        data = $(this).serialize();
        $.post(url, data, function (o) {
            $('#listInsert').append('<div>' + o.text + '<a class="del" rel="' + o.id + '" href="#">X</a></div>');
        }, 'json');
        return false;
    });
});