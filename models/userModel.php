<?php

class UserModel extends Model {

    public function __construct() {
        parent::__construct();
    }

    //function to select all users in a data base, returns it to controller
    public function userList(){
        return $this->db->select("SELECT user_id,first_name,last_name,email,birthDay,permission FROM user");
    }

    //function to create new user.
    public function create($data){
        //insert data to database(user_table), get last inserted ID to insert into equivalent table.
        $last_id = $this->db->insert('user', array('first_name' => $data['fname'],
                                        'last_name' => $data['lname'],
                                        'email' => $data['email'],
                                        'password' => Hash::create('md5',$data['pword'], HASH_KEY),
                                        'permission' => $data['permission']));
        //insert id to equilavent table(teahcer/student)
        if($data['permission'] == 'admin')
            $this->db->insert('teacher', array('teacherID' => $last_id));
        else
            $this->db->insert('student', array('studentID' => $last_id));
    }
    //delete user from database
    public function delete($id)
    {
        $permission = $this->db->select("SELECT permission FROM user WHERE user_id = $id");
        if($permission['info'][0]['permission'] == 'root')
            return false;
        $this->db->delete('user',"user_id = $id");
        if($permission['info'][0]['permission'] == 'admin')
        {
            $this->db->delete('teacher', "teacherID = $id");
            //$this->db->delete('teacher_notes', "teacherID = $id");
        }
        if ($permission['info'][0]['permission'] == 'default')
        {
            $this->db->delete('student', "studentID = $id");
            $this->db->delete('stud_group', "studentID = $id");
            $this->db->delete('stud_notes', "studentID = $id");
            $this->db->delete('student_answers', "studentID = $id");
        }
        return;
    }

    //select single student for editing in dashboard
    public function UserSingleList($id){
        $data = $this->db->select("SELECT user_id,first_name,last_name,email,birthDay,permission FROM user WHERE user_id = $id");
        if ($data['info'][0]['permission'] == 'default') {
            $level = $this->db->select("SELECT level FROM student WHERE studentID = $id");
            $data['info'][0]['level'] = $level['info'][0]['level'];
        }
        return $data;
    }
    //function to update user data
    public function editSave($data,$id){
        $postData = array('first_name' => $data['fname'],
                          'last_name' => $data['lname'],
                          'email' => $data['email'],
                          'password' => Hash::create('md5',$data['pword'], HASH_KEY),
                          'permission' => $data['permission']);

        $level = array('level' => $data['level']);
        $this->db->update('user', $postData, "`user_id` = {$id}");
        $this->db->update('student',$level,"studentID = '$id'");
        if($data['permission'] == 'admin')
        {
            $this->db->insert('teacher', array('teacherID' => $id));
            $this->db->delete('student', "studentID = $id");
        }
        else
            $this->db->insert('student', array('studentID' => $id));
    }

    //function to check if user already exists
    public function check($data){
        $result = $this->db->select("SELECT first_name,last_name,email FROM user WHERE first_name = '".$data['fname']."' OR last_name = '".$data['lname']."' OR email = '".$data['email']."'");
        return $result['count'];
    }

    public function temp($sql)
    {
        return $this->db->temp($sql);
    }
}