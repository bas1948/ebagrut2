<?php
class DashBoardModel extends Model {

        function __construct() {
          parent::__construct();
        }

        public function getAdminGroup($level,$teacherID){
                //Select data for single teacher
          return $this->db->select("SELECT u.user_id,u.first_name,u.last_name,u.email,s.progress,u.permission,s.level
            FROM stud_group g, student s, teacher t, user u
            WHERE g.level = '$level' AND t.teacherID = '$teacherID' AND g.studentID = u.user_id AND u.user_id = s.studentID");
        }

        public function getRootGroup(){
                //select data for all teachers
          return $this->db->select("SELECT u.user_id,u.first_name,u.last_name,u.email,s.progress,s.level,u.permission
                                    FROM user u, student s, teacher t, stud_group g
                                    WHERE g.studentID = u.user_id AND u.user_id = s.studentID
                                    UNION
                                    SELECT NULL as user_id,first_name,last_name,email,NULL as birthday,teacher.level,user.permission
                                    FROM user,stud_group, teacher
                                    WHERE stud_group.teacherID = teacher.teacherID AND teacher.teacherID = user.user_id");
        }

        public function getGrammarTable($level){
          return $this->db->select("SELECT excID,level,question
            FROM grammar_exc
            WHERE level = '$level'");
        }

        public function getReadingTable($level){
          return $this->db->select("SELECT excID,level,article_desc,article
            FROM reading_exc
            WHERE level = '$level'");
        }

        public function getUser($id){
          return $this->db->select("SELECT s.level,s.progress,u.first_name,u.last_name,u.user_id
            FROM user u, student s
            WHERE u.user_id = '$id' AND s.studentID = u.user_id");
        }

        public function updateProgress($id,$data){
          $this->db->update('student', $data, "`studentID` = {$id}");
        }

        public function addNote($data){
          return $this->db->insert('stud_notes',$data);
        }

        public function SelectSingleGrammar($id){
          return $this->db->select("SELECT * FROM grammar_exc WHERE excID = $id");
        }

        public function grammarEditSave($id,$data){
          $this->db->update('grammar_exc', $data, "`excID` = {$id}");
        }

        public function grammarAdd($data){
         return $this->db->insert('grammar_exc',$data); 
       }

       public function GrammarExcDelete($id){
        $this->db->delete("grammar_exc", "excID = $id");
      }

      public function readingAdd($data){
       return $this->db->insert('reading_exc',$data); 
      }

      public function getReadExc($id){
        return $this->db->select("SELECT * FROM reading_exc WHERE excID = $id");
      }

      public function readingEditSave($id,$data){
        $this->db->update('reading_exc', $data, "`excID` = {$id}");
      }

      public function readingExcDelete($id){
        $this->db->delete("reading_exc", "excID = $id");
      }

      public function getMsgTitle($id){
        return $this->db->select("SELECT title,noteID,is_read FROM stud_notes WHERE teacherID = $id AND NOT sentBy = $id");
      }

      public function msgContent($NoteId){
        $data = $this->db->select("SELECT s.studentID,s.title,s.content,s.date_added,s.sentBy,u.first_name,u.last_name FROM stud_notes s,user u WHERE s.noteID = $NoteId AND s.sentBy = u.user_id");
        if($data['info'][0]['sentBy'] == $data['info'][0]['studentID']) 
          $this->db->update('stud_notes',array("is_read" => "1"),"noteID = $NoteId");
        return $data;
      }
      
      public function DelNote($noteID){
        $this->db->delete("stud_notes", "noteID = $noteID");
      }

      public function getOutbox($id){
        return $this->db->select("SELECT title,noteID,is_read FROM stud_notes WHERE teacherID = $id AND sentBy = $id");
      }

      public function getStudAnswers($id){
        return $this->db->select("SELECT user.first_name,user.last_name,student_answers.*,reading_exc.question1,reading_exc.question2,reading_exc.question3,reading_exc.question4,reading_exc.question5
                                  FROM student_answers, reading_exc, user
                                  WHERE studentID = $id AND reading_exc.excID = student_answers.excID AND student_answers.studentID = user.user_id");
      }

      public function getExamsList(){
        return $this->db->select("SELECT * FROM exams");
      }

      public function uploadExam($data){
        $this->db->insert('exams',$data);
      }

}