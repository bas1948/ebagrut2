<?php

class studentModel extends Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function getGrammar($level){
        return $this->db->select("SELECT * FROM grammar_exc WHERE level='$level' LIMIT 15");
    }
    //set level and group
    public function setLevel($id,$level){
        //update level
        $this->db->update('student',array("level" => $level),"studentID = $id");
        //insert into group
        $this->db->insert('stud_group',array('studentID' => $id, 'level' => $level));
    }
    
    public function ReSelect($id){
        $data = $this->db->select("SELECT user_id,first_name,last_name,permission FROM user WHERE user_id = $id"); 
        $count = $data['count']; 
        $data = $data['info'];

        if($count == 1)
        {
            Session::set(array( 'permission' => $data[0]['permission'],
                'fname' => $data[0]['first_name'],
                'lname' => $data[0]['last_name'],
                'user_id' => $data[0]['user_id']));

            if($data[0]['permission'] == 'default')
            {
                $studentData = $this->GetStudentData($data[0]['user_id'])['info'][0];
                Session::set(array('progress' => $studentData['progress'], 
                                    'level' => $studentData['level'],
                                    'count_grammar' => $studentData['count_grammar'],
                                    'count_reading' => $studentData['count_reading']));
            }

        }
        else
        {
            return $error='select_fail';
        }
    }
    
    public function GetStudentData($id){
        return $this->db->select("SELECT * FROM student WHERE studentID = $id");
    }

    public function saveAnswers($data){
        $this->db->insert('student_answers',$data);
    }

    public function getMsgTitle($id){
        return $this->db->select("SELECT title,noteID,is_read FROM stud_notes WHERE studentID = $id AND NOT sentBy = $id");
    }

    public function msgContent($NoteId){
        $data = $this->db->select("SELECT s.teacherID,s.title,s.content,s.date_added,s.sentBy,u.first_name,u.last_name FROM stud_notes s,user u WHERE s.noteID = $NoteId AND s.sentBy = u.user_id");
        if($data['info'][0]['sentBy'] == $data['info'][0]['teacherID'])
            $this->db->update('stud_notes',array("is_read" => "1"),"noteID = $NoteId");
        return $data;
    }

    public function addNote($data){
        return $this->db->insert('stud_notes',$data);
    }

    public function getOutbox($id){
        return $this->db->select("SELECT title,noteID,is_read FROM stud_notes WHERE studentID = $id AND sentBy = $id");
    }

    public function getAdmins(){
        return $this->db->select("SELECT user_id,first_name,last_name FROM user WHERE permission = 'root' OR permission= 'admin'");
    }

    public function DelNote($noteID){
        $this->db->delete("stud_notes", "noteID = $noteID");
    }

    public function getReadingExc($level){
        return $this->db->select("SELECT * FROM reading_exc WHERE level='$level' LIMIT 1");
    }

    public function getGrammarExc($level){
        return $this->db->select("SELECT * FROM grammar_exc WHERE level='$level' LIMIT 10");
    }

    public function getExamList(){
        return $this->db->select("SELECT * FROM exams");
    }

    public function setGrammarProgress($id, $grammarCount,$progress){
        $this->db->update('student',array("progress" => $progress,'count_grammar' => $grammarCount),"studentID = $id");
    }

    public function setReadingProgress($id, $readingCount){
        $this->db->update('student',array('count_reading' => $readingCount),"studentID = $id");
    }
} 