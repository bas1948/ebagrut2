<?php

class registerModel extends Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function create($data){
        $last_id = $this->db->insert('user', array('first_name' => $data['fname'],
                                        'last_name' => $data['lname'],
                                        'email' => $data['email'],
                                        'password' => Hash::create('md5',$data['pword'], HASH_KEY),
                                        'permission' => $data['permission'],
                                        'birthDay' => $data['birthDay']
                                        ));
        $this->db->insert('student', array('studentID' => $last_id));
        return $last_id;
    }
    
    public function check($data){

        $result = $this->db->select("SELECT first_name,last_name,email FROM user WHERE first_name = '".$data['fname']."' OR last_name = '".$data['lname']."' OR email = '".$data['email']."'");

        return $result['count'];
    }
}