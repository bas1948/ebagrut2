<?php
class loginModel extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function run($email, $password){
        $data = $this->db->select("SELECT user_id,first_name,last_name,permission FROM user WHERE email = '$email' AND password = '$password'"); 
        $count = $data['count']; 
        $data = $data['info'];
        if($count == 1)
        {
            Session::set(array('loggedin' => true,
                               'permission' => $data[0]['permission'],
                                'fname' => $data[0]['first_name'],
                                'lname' => $data[0]['last_name'],
                                'user_id' => $data[0]['user_id']));

            if($data[0]['permission'] == 'default')
            {   
                $studentData = $this->GetStudentData($data[0]['user_id'])['info'][0];
                Session::set(array('progress' => $studentData['progress'], 
                                   'level' => $studentData['level'],
                                   'teacherID' => $this->GetTeacherID($data[0]['user_id']),
                                   'count_grammar' => $studentData['count_grammar'],
                                   'count_reading' > $studentData['count_reading']));

                return $data[0]['permission'];
            }
            else
                if($data[0]['permission'] == 'admin')
                    {
                        Session::set(array('level' => $this->GetTeacherLevel($data[0]['user_id'])));
                        return $data[0]['permission'];
                    }
                return $data[0]['permission'];
        }
        else
        {
            return $error='login_fail';
        }
    }

    public function GetStudentData($id){
        return $this->db->select("SELECT * FROM student WHERE studentID = $id");
    }

    public function GetTeacherLevel($id)
    {
        $level = $this->db->select("SELECT level
                                    FROM teacher
                                    WHERE '$id' = teacherID");

        return $level['info'][0]['level'];
    }

    public function GetTeacherID($id){
        $teacherID = $this->db->select("SELECT u.user_id
                                        FROM user u, stud_group g
                                        WHERE g.studentID = '$id' AND g.teacherID = u.user_id");

        return isset($teacherID['info'][0]['user_id']) ? $teacherID['info'][0]['user_id'] : null; 
    }
}